import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal( { 
    title: "Nailed!", 
    bodyElement: `${fighter.name}'s the winner!`,
    onClose: () => {
      window.location.reload();
    },
  } ); 
}

