import { controls } from '../../constants/controls';
import { fightersDetails } from '../helpers/mockData';

export async function fight(firstFighter, secondFighter) {
  // resolve the promise with the winner when fight is over
  console.log(firstFighter.name + ' vs. ' + secondFighter.name);
  
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;
  let healthBar1, healthBar2;
  let newHealthbar1, newHealthbar2;

  return new Promise((resolve) => {
    document.addEventListener('keydown', (event) => {
      let damage = getDamage(firstFighter, secondFighter);
  
      switch(event.code) {
        case controls.PlayerOneAttack:
            if (event.code == controls.PlayerOneBlock) {
              console.log("12345");
              break;
            }
            secondFighterHealth = secondFighterHealth - damage;

            healthBar1 = document.getElementById(`right-fighter-indicator`);
            newHealthbar1 = (secondFighterHealth / secondFighter.health) * 100;
            healthBar1.style.width = newHealthbar1 + "%";
  
            resolveWinner();
            break;
          case controls.PlayerTwoAttack:
              if (event.code == controls.PlayerTwoBlock) {
                break;
              }
            firstFighterHealth = firstFighterHealth - damage;

            healthBar2 = document.getElementById(`left-fighter-indicator`);
            newHealthbar2 = (firstFighterHealth / firstFighter.health) * 100;
            healthBar2.style.width = newHealthbar2 + "%";
  
            resolveWinner();
            break;
           
      }
    })

    function resolveWinner() {
      if(firstFighterHealth <= 0) {
        resolve(secondFighter);
      } else if(secondFighterHealth <= 0){
        resolve(firstFighter);
      }
    }

    runCombo(
      () => {
        console.log("Привет!");
        
        secondFighterHealth = secondFighterHealth - getComboDamage(firstFighter);
        healthBar1 = document.getElementById(`right-fighter-indicator`);
        newHealthbar1 = (secondFighterHealth / secondFighter.health) * 100;
        healthBar1.style.width = newHealthbar1 + "%";
      },
      ...controls.PlayerOneCriticalHitCombination
    );
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let damage = getHitPower(attacker) - getBlockPower(defender);
  
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  let hitPower = fighter.attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  let blockPower = fighter.defense * dodgeChance;

  return blockPower;
}

let recover = true;
function getComboDamage(attacker) {
  if(recover) {
    let damage = 2 * attacker.attack;
    recover = false;

    setTimeout( () => {
      recover = true;
    }, 2000)

    return damage;
  }
}

function runCombo(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', event => {
    pressed.add(event.code);
    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear();

    func();
  });

  document.addEventListener('keyup', event => {
    pressed.delete(event.code);
  });
}
