import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(fighter) {
    const fighterImg = createFighterImage(fighter);
    const fighterName = createElement({ 
      tagName: 'h3',
      className: 'fighter-name' 
    });
    const fighterDetails = createElement({
      tagName: 'div',
      className: 'fighter-details'
    });
    const fighterDetailsInfo = createElement({
      tagName: 'div',
      className: 'fighter-info'
    });
    fighterDetails.innerHTML = `
        <p>Attack: ${fighter.attack}</p>
        <p>Defense: ${fighter.defense}</p>
        <p>Health: ${fighter.health}</p>
    `;
    fighterName.innerText = fighter.name;
    fighterDetailsInfo.append(fighterName, fighterDetails);
    fighterElement.append(fighterImg, fighterDetailsInfo);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
